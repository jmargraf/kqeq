import sys
#sys.path.append("/datavon1/Development/new_gap/kqeq/")
sys.path.append("/home/mvondrak/workstation/Development/new_gap/kqeq")
from ase.io import read,write
from ase.units import Hartree

import numpy as np
import random
from qpac.GAP import GAP
from qpac.kernel import SOAPKernel
from qpac.utils import *
from qpac.descriptors import *
import matplotlib.pyplot as plt
from ase.visualize import view
from ase.units import Bohr, Hartree


mols = read("mols.xyz@:", format="extxyz")

atom_energy = {"Zn": -49117.02929728, "O": -2041.3604,
               "H": -13.63393, "Li": -7.467060138769*Hartree,
               "Na": -4421.77421255, "Cl": -12577.38265088}


SOAPclass = dscribeSOAP(nmax = 6,
                        lmax = 5,
                        rcut = 3.0,
                        sigma = 3.0/8,
                        species = ["H","O"],
                        periodic =False)

random.shuffle(mols)
train_set = mols[:100]
test_set = mols[100:150]

SOAP_Kernel = SOAPKernel(Descriptor=SOAPclass, 
                         training_set=train_set, #train_set,
                         sparse=True,
                         sparse_count=1000,
                         perEl = True,
                         sparse_method = "CURel")


my_kqeq = GAP(Kernel=SOAP_Kernel)

my_kqeq.train(lambda_reg=0.1,atom_energy=atom_energy)

E_test = []
for a in test_set:
    E_test.append(my_kqeq.calculate(a)["energy"]/len(a))
E_ref_test = get_energies_perAtom(mols=test_set,atom_energy = atom_energy) 
E_train = []
for a in train_set:
    E_train.append(my_kqeq.calculate(a)["energy"]/len(a))
E_ref_train = get_energies_perAtom(mols=train_set,atom_energy = atom_energy) 

plt.plot(E_ref_test,E_ref_test)
plt.scatter(E_ref_test,E_test)
plt.show()
plt.close()

plt.plot(E_ref_train,E_ref_train)
plt.scatter(E_ref_train,E_train)
plt.show()
plt.close()
