import sys
sys.path.append("/datavon1/kQeqDevelopment/cleaning/kqeq")


import time
import random
import matplotlib.pyplot as plt

from kqeq.qeq import charge_eq
from kqeq.kernel import SOAPKernel
from kqeq.kQEq import kernel_qeq
from kqeq.funct import *

import ase
from ase.units import Hartree
from ase import Atoms
from ase.io import read, write

import numpy as np
from sklearn.metrics import mean_squared_error
import math

dataset = read("NaClall.xyz@:",format="extxyz")

train_set, val_set, test_set = prep_structures(dataset,150,0, 50)

atom_energy = {"Zn": -49117.02929728, "O": -2041.3604,
               "H": -13.63393, "Li": -7.467060138769*Hartree}

hard_lib = {"Na": 2, "Cl": 2, "Cu": 1, "O": 1, "Zn": 1, "H": 0, "Li":0}

desdict = {"nmax": 4,
           "lmax": 3,
           "rcut": 3.0,
           "sigma": 3.0 / 6,
           "species":["Na","Cl"],
           "periodic": True}

SOAP_Kernel = SOAPKernel(multi_SOAP=False,
                     descriptor_dict=desdict,
                     training_set=train_set,
                     training_system_charges=[0 for a in train_set])

my_kqeq = kernel_qeq(Kernel=SOAP_Kernel,
                     scale_atsize=1.0,
                     radius_type="qeq",
                     sparse=True,
                     sparse_count=1000,
                     hard_lib=hard_lib,
                     periodic = True)

my_kqeq.train(target_lambdas =[0.00001],#,0.01], 
              targets = ["charges"],#,"charges"], 
              charge_keyword="hirshfeld")

#E_test = []
#for a in test_set:
#        E_test.extend(my_kqeq.calculate(a)["charges"])
#ref_en_test = get_energies_perAtom(mols=test_set,atom_energy = atom_energy)

kqeq_dipoles_train,kqeq_charges_train, _ = my_kqeq.predict(train_set)
kqeq_dipoles_test,kqeq_charges_test, _ = my_kqeq.predict(test_set)
c_ref_train = get_charges(train_set,charge_keyword="hirshfeld")
c_ref_test = get_charges(test_set,charge_keyword="hirshfeld")
# print(kqeq_charges_test)
plot_basics(kqeq_charges_test,c_ref_test,title="test",preset="charges")
plot_basics(kqeq_charges_train,c_ref_train,title="train",preset="charges")
# plot_basics(E_test,c_ref_test,preset="charges")
# plot_basics(E_test,kqeq_charges_test,preset="charges")
