import unittest
import sys
from ase.io import read
import numpy as np
from ase.units import Hartree
from qpac.kQEq import kernel_qeq
from qpac.utils import get_dipoles
from qpac.kernel import qpacKernel
from qpac.descriptors import quipSOAP
import matplotlib.pyplot as plt
import random
random.seed(10)
import os

script_dir = os.path.dirname(os.path.abspath(__file__))
filename = 'WaterMinima3_8.xyz'
file_path = os.path.join(script_dir, filename)


mols = read(f"{file_path}@:",format='extxyz')
random.shuffle(mols)
mols_train = mols[:50]
mols_test = mols[50:]
hard_lib = {"H":0.1*Hartree,"O":0.1*Hartree}


class TestKqeq(unittest.TestCase):    
    def test_prediction(self):
        desdict = {"nmax" : 3,
                   "lmax" : 2,
                   "rcut" : 2.5,
                   "sigma": 2.5/8,
                   "periodic": False}
        
        atsize = 1.0
        radtype = "qeq"
        species = np.unique([spec  for mol in mols for spec in  mol.get_chemical_symbols()])
        soap_desctipor = quipSOAP(STRsoap="soap cutoff=2.8 l_max=5 n_max=6 atom_sigma=1.0 normalize=T n_species=2 species_Z={1 8} n_Z=2 Z={1 8}", species=["H","O"])
        
        
        qpac_kernel = qpacKernel(Descriptors=soap_desctipor,
                         training_set=mols_train,
                         training_set_charges=[0 for a in mols_train],
                         sparse = True,
                         perEl = True,
                         sparse_method = "CUR",
                         sparse_count=200)
        my_kqeq = kernel_qeq(Kernel=qpac_kernel,
                     scale_atsize=1/np.sqrt(2.0),
                     radius_type="qeq")

        my_kqeq.train(target_sigmas=[0.000005],
                      targets = ["dipole"],
                      charge_keyword="hirshfeld")
        res_train = my_kqeq.predict(mols_train)
        res_test = my_kqeq.predict(mols_test)
        dipoles_train_ref = get_dipoles(mols_train)
        dipoles_test_ref = get_dipoles(mols_test)
##        print(np.max(np.subtract(dipoles_train, dipoles_train_ref)))
#        print(np.max(np.subtract(dipoles_test, dipoles_test_ref)))
        self.assertTrue(np.allclose(res_train["dipoles"], dipoles_train_ref,atol=0.1))
        # self.assertTrue(np.allclose(res_test["dipoles"], dipoles_test_ref,atol=0.5))
        
        

        
if __name__ == '__main__':
    unittest.main()

