import unittest
import syspip
from ase.io import read
import numpy as np
from ase.units import Hartree
from qpac.kQEq import kernel_qeq
from qpac.utils import get_charges
from qpac.kernel import qpacKernel
from qpac.descriptors import dscribeSOAP
import matplotlib.pyplot as plt
import random
random.seed(10)

mols = read("WaterMinima3_8.xyz@:",format='extxyz')
random.shuffle(mols)
mols_train = mols[:100]
mols_test = mols[50:]
hard_lib = {"H":0.1*Hartree,"O":0.1*Hartree}
charges_train_ref = get_charges(mols_train,"hirshfeld")
charges_test_ref = get_charges(mols_test,"hirshfeld")

class TestKqeq(unittest.TestCase):    
    def test_prediction(self):
        atsize = 1.0
        radtype = "qeq"
        SOAPclass = dscribeSOAP(nmax = 4,
                                lmax =3,
                                rcut = 3.6,
                                sigma = 3.6/8,
                                species = ["H","O"],
                                delta=1,
                                periodic = False)
        
        Kernel1 = qpacKernel(Descriptors=[SOAPclass],#SOAPclass,
                               training_set=mols_train,
                               sparse=True,
                               sparse_method="CURel",
                               sparse_count=500,
                               perEl = True)
        my_kqeq = kernel_qeq(Kernel=Kernel1,
                             scale_atsize=1.0/np.sqrt(2),
                             radius_type="qeq",
                             hard_lib=hard_lib,
                             periodic = False)
        my_kqeq.train(target_lambdas=[0.000005],
                      targets = ["charges"],
                      charge_keyword="hirshfeld")
        _,charges_train ,_ = my_kqeq.predict(mols_train)
        _,charges_test,_ = my_kqeq.predict(mols_test)
        self.assertTrue(np.allclose(charges_train, charges_train_ref,atol=0.035))
        self.assertTrue(np.allclose(charges_test, charges_test_ref,atol=0.055))
        assert Kernel1.train_descs[0]["H"].shape == (2172,144) 
        assert Kernel1.train_descs[0]["O"].shape == (2172,144) 
        assert Kernel1.repres_descs[0]["H"].shape == (1000,144) 
        assert Kernel1.repres_descs[0]["O"].shape == (1000,144) 
        
if __name__ == '__main__':
    unittest.main()

