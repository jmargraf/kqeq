q-pac
================================

q-pac is a Python implementation of the kernel Charge Equilibration method (more `here <https://iopscience.iop.org/article/10.1088/2632-2153/ac568d/meta>`_). This allows training and using physics-based machine-learning models for predicting charge distributions in molecules and materials.

This documentation contains some tutorials and the API documentation (in progress).

Installation
============

This package has been tested with python 3.9. External dependencies are `numpy <https://numpy.org>`_ (for linear algebra), `ase <https://wiki.fysik.dtu.dk/ase/>`_ (for handling structural data) and `DScribe <https://singroup.github.io/dscribe/latest/>`_ (for calculating atomic environment representations). The kQEq package and its dependencies can be installed using ``python setup.py install`` 


Contents
========
.. toctree::

    tutorialmu
    tutorialE

  
Changelog
=========
- 2.0:
    - Potentials for ionic systems 
- 1.0:
    - Initial release.
