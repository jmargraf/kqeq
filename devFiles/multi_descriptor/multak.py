import sys
sys.path.append("/datavon1/Development/FinallyTurboSoap/")

from qpac.descriptors import dscribeSOAP,quipSOAP
from qpac.kernel import *
from qpac.utils import *
from qpac.GAP import kernel_qeq

from ase.units import Hartree
from ase.io import read, write

import numpy as np

import random
random.seed(10)

def get_cutoff_padding(sigma):
    threshold = 0.001
    cutoff_padding = sigma * np.sqrt(-2 * np.log(threshold))
    return cutoff_padding

def addPeriodicity(mols):
    for mol in mols:
        xyz = mol.get_positions()
        x = abs(max(xyz[:,0]) - min(xyz[:,0]))
        y = abs(max(xyz[:,1]) - min(xyz[:,1]))
        z = max(xyz[:,2]) -  min(xyz[:,2])
        mol.set_cell((x*10, y*10, z*10))
        mol.set_pbc((True, True, True))
        mol.center()


# Commented structures are the ones used for the training and evaluation of the final model in the paper. This requires larger amount of RAM and the training time 
temp = read("water.xyz@:",format="extxyz")
random.shuffle(temp)
train_set = temp[:40]
test_set = temp[40:60]
nAt = 0
nAt_test = 0
addPeriodicity(test_set)
addPeriodicity(train_set)

for a in test_set:
    nAt_test += len(a)
for a in train_set:
    nAt += len(a)
print(f"number of atoms: {nAt}")

# atom_energy = {"O": -2034.814700117 , "Li": -201.019351696, "Cl": -12558.489966372}
atom_energy = {"O": -2041.3604, "H": -13.63393}
hard_lib = {"O": 0.2*Hartree, "H": 0.1*Hartree}

sigma = 2.6
trans = get_cutoff_padding(sigma)
# Desc1 = quipSOAP(STRsoap=f"soap cutoff={sigma} l_max=2 n_max=3 atom_sigma={sigma/6} radial_scaling=0.5 cutoff_transition_width={trans} normalize=T central_weight=1.0", species=["O","H"])

Desc1 = quipSOAP(STRsoap="soap cutoff=2.6 l_max=2 n_max=3 atom_sigma=0.33 normalize=T n_species=2 species_Z={1 8} n_Z=2 Z={1 8}", species=["O","H"])
Desc2 = quipSOAP(STRsoap="soap cutoff=2.6 l_max=2 n_max=5 atom_sigma=1.5 normalize=T n_species=2 species_Z={1 8} n_Z=1 Z={1 8}", species=["O","H"])

'''
Desc1 = dscribeSOAP(nmax = 3,
                   lmax =2,
                    rcut = 2.6,
                    sigma = 2.6/8,
                    species = ["H","O"],
                    delta = 0.5,
                    periodic = False)

Desc2 = dscribeSOAP(nmax = 5,#10,
                    lmax =3 ,
                    rcut = 5.0,
                    sigma = 5.0/8,
                    species = ["H","O"],
                    delta = 0.8,
                    periodic = False)
'''

# SOAPclass.compute(train_set,"training")
# print(SOAPclass.precomputed["training"])
# print("HHHHHHHHHHHHHHHHHHHHHHHHHHH")
# SOAPclass.saveSOAP("training","train_set20.npy")
# SOAPclass.loadSOAP("training","train_set5000.npy")


multiKer = MultipleSoap(Descriptors=[Desc1],
                        training_set=train_set,
                        sparse=True,
                        sparse_method="CURel",
                        sparse_count=200,
                        perEl = True)
my_kqeq = kernel_qeq(Kernel=multiKer,
                     scale_atsize=1.0/np.sqrt(2),
                     radius_type="qeq",
                     hard_lib=hard_lib)
my_kqeq.train(target_lambdas =[0.00001],#,0.01], 
              targets = ["charges"],#,"charges"], 
              charge_keyword="hirshfeld")

'''
print(len(multiKer.representing_set_descriptors[0]))
print(len(multiKer.representing_set_descriptors[1]))
K_nm,K_mm = multiKer.kernel_matrices(kerneltype="training")
simp1 =  SOAPKernel(Descriptor=Desc1,
                    training_set=train_set,
                    sparse=True,
                    sparse_method="CURel",
                    sparse_count=200,
                    perEl = True)
simp2 =  SOAPKernel(Descriptor=Desc2,
                    training_set=train_set,
                    sparse=True,
                    sparse_method="CURel",
                    sparse_count=200,
                    perEl = True)
knn1,kmm1 = simp1.kernel_matrix(kerneltype="training")
knn2,kmm2 = simp2.kernel_matrix(kerneltype="training")

'''

# SOAP_Kernel.saveSparse()

# Energy of the training set
ref_en_train = get_energies(mols=train_set,atom_energy = atom_energy)
ref_c = get_charges(test_set,"hirshfeld")
d,c,_ = my_kqeq.predict(test_set)
plot_basics(ref_c,c,preset="charges")

