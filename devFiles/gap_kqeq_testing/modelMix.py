import sys
sys.path.append("/datavon1/Development/FinallyTurboSoap/")

from qpac.descriptors import dscribeSOAP, quipSOAP
from qpac.kernel import *
from qpac.utils import *
from qpac.kQEq  import kernel_qeq
from qpac.GAP import GAP
from qpac.MLmodel import MLmodel


from ase.units import Hartree
from ase.io import read, write

import numpy as np


def addPeriodicity(mols):
    for mol in mols:
        xyz = mol.get_positions()
        x = abs(max(xyz[:,0]) - min(xyz[:,0]))
        y = abs(max(xyz[:,1]) - min(xyz[:,1]))
        z = max(xyz[:,2]) -  min(xyz[:,2])
        mol.set_cell((x*10, y*10, z*10))
        mol.set_pbc((True, True, True))
        mol.center()

temp = read("particles4.xyz@:",format="extxyz")
train_set = temp[:50]
nAt = 0
for a in train_set:
    nAt += len(a)
print(nAt)
test_set = temp[100:150]
#test_set = read("particles5.xyz@:",format="extxyz")
addPeriodicity(train_set)
addPeriodicity(test_set)

atom_energy = {"Zn": -49117.02929728, "O": -2041.3604}

hard_lib = {"O": 1*Hartree, "Zn": 1*Hartree}


Desc1 = quipSOAP(STRsoap="soap cutoff=2.6 l_max=5 n_max=6 atom_sigma=0.325 normalize=T n_species=2 species_Z={8 30} n_Z=2 Z={8 30}", species=["O","Zn"])
Desc2 = quipSOAP(STRsoap="soap cutoff=3.5 l_max=3 n_max=6 atom_sigma=0.7 normalize=T n_species=2 species_Z={8 30} n_Z=2 Z={8 30}", species=["O","Zn"])
Desc3 = quipSOAP(STRsoap="soap cutoff=3.6 l_max=2 n_max=8 atom_sigma=0.7 normalize=T n_species=2 species_Z={8 30} n_Z=2 Z={8 30}", species=["O","Zn"])
Desc4 = quipSOAP(STRsoap="soap cutoff=3.0 l_max=4 n_max=4 atom_sigma=0.6 normalize=T n_species=2 species_Z={8 30} n_Z=2 Z={8 30}", species=["O","Zn"])



Kernel1 = MultipleSoap(Descriptors=[Desc1],#SOAPclass,
                     training_set=train_set,
                     sparse=True,
                     sparse_method="CURel",
                     sparse_count=50,
                     perEl = True)
Kernel2 = MultipleSoap(Descriptors=[Desc2,Desc4],#SOAPclass,
                     training_set=train_set,
                     sparse=True,
                     sparse_method="CURel",
                     sparse_count=30,
                     perEl = True)

# Energy of the training set
ref_en_train = get_energies(mols=train_set,atom_energy = atom_energy)
my_kqeq = kernel_qeq(Kernel=Kernel1,
                     scale_atsize=1.0/np.sqrt(2),
                     radius_type="qeq",
                     hard_lib=hard_lib)
my_gap = GAP(Kernel=Kernel2)

testing_structure = MLmodel(my_gap,my_kqeq)
testing_structure.trainEn(lambdaE=10,
                        charge_keyword="hirshfeld",
                        atom_energy = atom_energy,
                        energy_keyword = "energy")
E_train = []
q_train = []
for m in train_set:
    res = testing_structure.calculateEQ(m)
    E_train.append(res["energy"]/len(m))
    q_train.extend(res["charges"]/len(m))

ref_en_train = get_energies_perAtom(mols=train_set,atom_energy = atom_energy)
plt.figure(figsize=(10,8), dpi=100)
plt.plot(ref_en_train,ref_en_train,alpha = 0.5,color="black")
plt.scatter(E_train,ref_en_train,alpha = 0.5)
plt.xticks(fontsize=18)
plt.yticks(fontsize=18)
plt.show()
