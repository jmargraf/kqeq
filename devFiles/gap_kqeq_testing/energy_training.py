import sys
sys.path.append("/datavon1/Development/FinallyTurboSoap/")

from qpac.descriptors import dscribeSOAP, quipSOAP
from qpac.kernel import *
from qpac.utils import *
from qpac.kQEq import kernel_qeq

from ase.units import Hartree
from ase.io import read, write

import numpy as np


def addPeriodicity(mols):
    for mol in mols:
        xyz = mol.get_positions()
        x = abs(max(xyz[:,0]) - min(xyz[:,0]))
        y = abs(max(xyz[:,1]) - min(xyz[:,1]))
        z = max(xyz[:,2]) -  min(xyz[:,2])
        mol.set_cell((x*10, y*10, z*10))
        mol.set_pbc((True, True, True))
        mol.center()

# Commented structures are the ones used for the training and evaluation of the final model in the paper. This requires larger amount of RAM and the training time 
'''
train_set = read("particles.xyz@:",format="extxyz")
temp = read("particles1.xyz@:", format="extxyz")
train_set.extend(temp[:50])
temp = read("particles2.xyz@:", format="extxyz")
train_set.extend(temp[:100])
temp = read("particles3.xyz@:", format="extxyz")
train_set.extend(temp[:200])
temp = read("particles4.xyz@:", format="extxyz")
train_set.extend(temp[:200])

test_set = read("particles5.xyz@:", format="extxyz")[:200]
'''
##########################################################
temp = read("particles4.xyz@:",format="extxyz")
train_set = temp[:50]
test_set = temp[100:150]
#test_set = read("particles5.xyz@:",format="extxyz")
addPeriodicity(train_set)
addPeriodicity(test_set)

atom_energy = {"Zn": -49117.02929728, "O": -2041.3604}

hard_lib = {"O": 1*Hartree, "Zn": 1*Hartree}


SOAPclass = dscribeSOAP(nmax = 6,
                        lmax =5 ,
                        rcut = 3.0,
                        sigma = 3.0/8,
                        delta=1.0,
                        species = ["Zn","O"],
                        periodic = False)


Desc1 = quipSOAP(STRsoap="soap cutoff=3.0 l_max=5 n_max=6 atom_sigma=0.5 normalize=T n_species=2 species_Z={8 30} n_Z=2 Z={8 30}", species=["O","Zn"])
Desc2 = quipSOAP(STRsoap="soap cutoff=5.0 l_max=3 n_max=8 atom_sigma=1 normalize=T n_species=2 species_Z={8 30} n_Z=2 Z={8 30}", species=["O","Zn"])



SOAP_Kernel = MultipleSoap(Descriptors=[Desc1,Desc2],#SOAPclass,
                     training_set=train_set,
                     sparse=True,
                     sparse_method="CURel",
                     sparse_count=500,
                     perEl = True)

# Energy of the training set
ref_en_train = get_energies(mols=train_set,atom_energy = atom_energy)

# Create an instance of the kQEq class
my_kqeq = kernel_qeq(Kernel=SOAP_Kernel,
                     scale_atsize=1.0/np.sqrt(2),
                     radius_type="qeq",
                     hard_lib=hard_lib)

#print(my_kqeq))
if type(my_kqeq) == kernel_qeq:
    print("Hello worldHello worldHello worldHello worldHello worldHello worldHello worldHello worldHello worldHello worldHello worldHello worldHello worldHello worldHello worldHello worldHello worldHello worldHello worldHello world")
'''
my_kqeq.trainEn(atom_energy=atom_energy,
                      charge_keyword="hirshfeld", 
                      lambda_reg = 0.01,
                      iter_charges = 2)

'''

