import numpy as np


def x2(x):
    return x*x

def dx2(x):
    return 2*x

r= 5
origA = np.array([[2.4,4.2],[3.2,1.2]])
A = origA*(r**2)
dA = origA*(2*r)
invA = np.linalg.inv(A)

def num_grad(M, h=0.0001):
    # temp = M.copy()
    temp = np.linalg.inv(M)
    r_temp = 5
    r_temp += h
    Eplus = temp*(1/(r_temp**2))
    r_temp -= 2*h
    Eminus = temp*(1/(r_temp**2))
    energy = (Eplus - Eminus) / (2.0 * h)
    return energy  

temp = np.matmul(-1*invA,dA)
temp = np.matmul(temp,invA)
print(temp/num_grad(origA))
