import sys
sys.path.append("/home/mvondrak/development/qeqInv/qpac")


from qpac.descriptors import dscribeSOAP, quipSOAP
from qpac.kernel import *
from qpac.utils import *
from qpac.kQEq import kernel_qeq

from ase.units import Hartree
from ase.io import read, write
import numpy as np




mols = read("trainWater.xyz@:",format="extxyz")
random.seed(10)
random.shuffle(mols)
train_set = mols[:10]
test_set = mols[5:10]
nAt = 0 
for a in train_set:
    nAt += len(a)
print(f"Number of atoms in the training set: {nAt} in {len(train_set)}")
addPeriodicity(train_set)
addPeriodicity(test_set)
random.seed(10)
atom_energy = {"H": -12.6746244439181, "O": -2041.03979050724}
soap_descriptor = quipSOAP(STRsoap="soap cutoff=3.0 l_max=5 n_max=6 atom_sigma=0.5 normalize=T n_species=2 species_Z={1 8} n_Z=2 Z={1 8}", species=["H","O"])
qpac_kernel = qpacKernel(Descriptors=soap_descriptor,
                         training_set=train_set,
                         training_set_charges=[0 for a in train_set],
                         sparse = True,
                         perEl = True,
                         sparse_method = "CUR",
                         sparse_count=500)



my_kqeq = kernel_qeq(Kernel=qpac_kernel,
                      scale_atsize=1.0/np.sqrt(2.0),
                      radius_type="qeq")
my_kqeq.train(
    # target_sigmas =[0.01],#,0.01], 
    # targets = ["dipole"],#,"charges"], 
    atom_energy=atom_energy,
    target_sigmas =[0.0001,0.0001],#,0.01], 
    targets = ["energy"],#["charges","dipole"],#,"charges"], 
    charge_keyword="hirshfeld",
    scc_target=False,
    n_bad_iters=5,
    verbose=False,
    max_iter = 5)


def num_grad(mol,my_kqeq,h=0.0001,direction=0,iatom=0):
    tmpmol = mol.copy()
    pos = tmpmol.get_positions()#/Bohr
    pos[iatom][direction] += h
    tmpmol.set_positions(pos)#*Bohr)
    res = my_kqeq.calculate(tmpmol)
    Eplus = res['energy']#/Hartree
    pos[iatom][direction] += -2.0*h
    tmpmol.set_positions(pos)#*Bohr)
    res = my_kqeq.calculate(tmpmol)
    Eminus = res['energy']#/Hartree
    pos[iatom][direction] += h
    tmpmol.set_positions(pos)#*Bohr)
    energy = (Eplus-Eminus)/(2.0*h)#*Bohr)
    return energy#*Hartree#/Bohr
    
def num_grads(mol,my_kqeq,h=0.0001):
    f = np.zeros((len(mol),3))
    for i in range(len(mol)):
        for direction in range(3):
            f[i,direction] = -num_grad(mol,my_kqeq,h=h,direction=direction,iatom=i)
    return f
results_num = num_grads(test_set[4],my_kqeq)
results = my_kqeq.calculate(test_set[4])["forces"]
print(results_num/results)
