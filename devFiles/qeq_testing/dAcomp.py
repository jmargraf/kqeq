import sys 
sys.path.append("/home/mvondrak/development/qeqInv/qpac/")
from ase.units import Bohr,Hartree
from ase.io import read
import numpy as np
from qpac.qeq import charge_eq
from qpac.utils import addPeriodicity


mol = read("NaCltrain2.xyz@0",format="extxyz")
qe = charge_eq(mol,radius_type="rcov",scale_atsize=1.0,periodic=False)
dA = qe.get_dAdr()

def num_grad(mol, h=0.0001, direction=0, iatom=0):
    tmpmol = mol.copy()
    pos = tmpmol.get_positions()  # /Bohr
    pos[iatom][direction] += h*Bohr
    tmpmol.set_positions(pos)  # *Bohr)
    qe = charge_eq(tmpmol, radius_type="rcov", scale_atsize=1.0, periodic=False)
    res = qe.get_A()
    Eplus = res
    pos[iatom][direction] -= 2.0 * h*Bohr
    tmpmol.set_positions(pos)  # *Bohr)
    qe = charge_eq(tmpmol, radius_type="rcov", scale_atsize=1.0, periodic=False)
    res = qe.get_A()
    Eminus = res
    pos[iatom][direction] += h*Bohr
    tmpmol.set_positions(pos)  # *Bohr)
    energy = (Eplus - Eminus) / (2.0 * h)  # *Bohr) *Hartree#/Bohr
    return energy

def num_grads(mol, h=0.0001):
    n_atoms = len(mol)  
    f = np.zeros((n_atoms, n_atoms, 3))  
    for i in range(n_atoms):
        for direction in range(3):
            gradient_matrix = num_grad(mol, h=h, direction=direction, iatom=i)
            f[i, :, direction] = gradient_matrix[i, :] 

    return f

print(num_grads(mol)/dA)
