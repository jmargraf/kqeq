import numpy as np

from ase.io import *
from ase.data import covalent_radii
from ase.units import Bohr, Hartree
import random
# from quippy.descriptors import Descriptor
from qpac.data import atomic_numbers
from qpac.utils import get_energies
from qpac.kQEq import kernel_qeq


class GAP():
    """Class for Kernel Charge Equilibration Models

    Parameters
    ----------
        Kernel: obj
            Kernel object with the function kernel_matrix
        scale_atsize: float
            Scaling factor for covalent radii, used to define atom sizes
        radius_type: string
            'rcov' or 'qeq' (default)
        sparse: bool
            Setting training to use full or sparse training set
        sparse_count: int
            Number of training points used for sparsed training
        sparse_method: string
            Method used to specify representative set "FPS", or "CUR" (default) 
    """

    def __init__(self, Kernel=None, scale_atsize=1.0, sparse=False, sparse_count = 1000, sparse_method = "CUR"):
        
        if Kernel == None:
            print("Specify kernel!")
            exit()
        # inputs and defaults
        self.scale_atsize = scale_atsize
        self.Kernel       = Kernel
        self.weights      = None
        self.sparse       = sparse
        if self.sparse == True:
            self.sparse_method = sparse_method
            if sparse_count == None:
                if  self.K_train.shape[0] < 1000:
                    self.sparse_count =  self.K_train.shape[0]
                    print("Training set has less than 1000 points, training will be done with the full set")
                else:
                    self.sparse_count = 1000
            else:
                self.sparse_count = int(sparse_count)
            if sparse_method == "CUR":
                self.sparse_points = self.Kernel._create_representationCUR(self.sparse_count)
                #self.Kernel._create_representationCUR(self.sparse_count)
            elif sparse_method == "CURel":
                self.sparse_points = self.Kernel._create_representationCUR_elements(self.sparse_count)
                #self.Kernel._create_representationCUR_elements(self.sparse_count)
            elif sparse_method == "CURcov":
                self.sparse_points = self.Kernel._create_CUR_covariance(self.sparse_count)
            elif sparse_method == "CURcovel":
                self.sparse_points = self.Kernel._create_CUR_covariance_elements(self.sparse_count)
            elif sparse_method == "prepicked":
                self.sparse_points = sparseSelect
                self.Kernel._create_representationPrepicked(sparseSelect)
            else:
                print("Specify sparse method!")
            self.K_nm, K_mm = self.Kernel.kernel_matrix(kerneltype='training')
            for a in range(K_mm.shape[0]):
                K_mm[a,a] = K_mm[a,a] + 0.0000001 
            self.K_mm = K_mm
        else:
            self.Kernel.representing_set_descriptors = self.Kernel.training_set_descriptors
            self.K_nm, K_mm = self.Kernel.kernel_matrix(kerneltype='training')
            for a in range(K_mm.shape[0]):
                K_mm[a,a] = K_mm[a,a] + 0.0000001 
            self.K_mm = K_mm



    def train(self, lambda_reg =0.1, perAtom = False, atom_energy = None, energy_keyword = "energy"):
        L = np.zeros((len(self.Kernel.training_set_descriptors),len(self.Kernel.training_set)))
        count_c = 0
        count_r = 0
        E_ref = get_energies(mols=self.Kernel.training_set,atom_energy = atom_energy, energy_keyword = energy_keyword)
        for mol in self.Kernel.training_set:
            for at in mol:
                L[count_r,count_c] = 1
                count_r += 1
            count_c += 1
        K_nm = self.K_nm
        K_mm = self.K_mm   
        Lambda = np.linalg.inv(lambda_reg*np.eye(len(E_ref)))
        up = np.linalg.multi_dot([K_nm.T,L,Lambda,E_ref])
        down = np.linalg.multi_dot([K_nm.T, L,Lambda,L.T,K_nm]) + K_mm
        weights = np.linalg.solve(down,up)
        self.weights = weights

        

    def calculate(self, mol):
        #K = self.Kernel.kernel_matrix(mol_set1 = [mol],kerneltype="predicting")
        K,dKdr  = self.Kernel.calculate_function(mol_set1 = mol)
        energy = np.matmul(K,self.weights)
        nAt  = len(mol)
        f_k  = np.zeros((nAt,3))
        for j in range(nAt):
            for direction in range(3):
                eneg_drj = np.matmul(dKdr[j][direction],self.weights)
                for i in range(nAt):
                    f_k[j,direction] += -eneg_drj[i]
        results = {'energy':np.sum(energy)*Hartree,'forces':f_k * Hartree}
        return results

    def write_sparse_xml(self):
        with open("GAP.xml.314","w") as xml:
            for atom in self.Kernel.representing_set_descriptors:
                for el in atom:
                    xml.write(f"{str(el)}\n")
            
    def write_xml_weights(self):
        id = 1
        with open("GAP.xml","w") as xml:
            for atom in self.weights:
                xml.write(f'<sparseX i="{id}" alpha="{atom}" sparseCutoff="1.0000000000000000"/>\n')
                id += 1


    def save_model(self,name="kqeq", gap_version=1631710424, atom_energy=[]):
        sparse_len = len(self.Kernel.representing_set_descriptors)
        sparse_dim = len(self.Kernel.representing_set_descriptors[0])
        with open(f"{name}.xml.sparseX.kqeq","w") as xml:
            for atom in self.Kernel.representing_set_descriptors:
                for el in atom:
                    xml.write(f"{str(el)}\n")
        with open(f"{name}.xml","w") as xml:
            xml.write(f"<{name}>\n")
            xml.write(f'<Potential label="{name}" init_args="IP GAP label={name}"/>\n')
            xml.write(f'<GAP_params label="{name}" gap_version="{gap_version}">\n')
            xml.write(f'  <GAP_data do_core="F">\n')
            for element in atomic_numbers:
                if element not in atom_energy:
                    xml.write(f'    <e0 Z="{atomic_numbers[element]}" value=".00000000000000000E+000"/>\n')
                else:
                    xml.write(f'    <e0 Z="{atomic_numbers[element]}" value="{atom_energy[element]}"/>\n')
            xml.write(f'  </GAP_data>\n')
            xml.write(f'  <gpSparse label="{name}" n_coordinate="1">\n')
            xml.write(f'    <gpCoordinates label="{name}1" dimensions="{sparse_dim}" signal_variance="1.0" signal_mean=".00000000000000000E+000" sparsified="T" n_permutations="1" covariance_type="2" zeta="2.0000000000000000" n_sparseX="{sparse_len}" sparseX_filename="{name}.xml.sparseX.kqeq" sparseX_md5sum="2519cb796dd238f6aaf967cdf0dbb96e">\n')
            xml.write(f'            <descriptor>{self.Kernel.Descriptor_description}</descriptor>\n')
            id = 1
            for atom in self.weights:
                xml.write(f'      <sparseX i="{id}" alpha="{atom}" sparseCutoff="1.0000000000000000"/>\n')
                id += 1
            xml.write(f'    </gpCoordinates>\n')
            xml.write(f'  </gpSparse>\n')
            xml.write(f'</GAP_params>\n')
            xml.write(f'</{name}>\n')



class _GAPkQEq(kernel_qeq):

    def trainOld(self, lambda_reg = 0.1, 
                    lambda_charges = 0.001, 
                    iter_charges = 3, 
                    lambda_charges_min = 1e-6, 
                    charge_keyword='initial_charges', 
                    energy_keyword = "energy",
                    atom_energy = None):
        '''
        A is matrix from kQEq, so inverse hardness (A_up is zero matrix from GAP to this matrix)
        A_ones is matrix with ones and zeros which goes together with Q matrix from kQEq for energy
        At this moment only on additional target is allowed
        '''
        oldRMSE = 10000000
        K_nm = self.K_nm
        K_mm = self.K_mm        
        fullK_nm = np.kron(np.eye(2),K_nm)
        fullK_mm = np.kron(np.eye(2),K_mm)
        E_ref = get_energies(mols=self.Kernel.training_set,atom_energy = atom_energy, energy_keyword = energy_keyword)
        A, X, O, R, Xback = self._build_prediction(self.Kernel.training_set, self.Kernel.training_system_charges)
        if iter_charges > 0:
            T, ref = self._process_data("charges",self.Kernel.training_set,charge_keyword)
            nMol = len(self.Kernel.training_set)
            A_up = np.zeros((A.shape[0],A.shape[1]-nMol))
            fullA = np.hstack((A_up,A))
            X_up = np.zeros((X.shape[0]-nMol,X.shape[1]))
            fullX_up = np.hstack((X_up,X_up))
            fullX_temo = np.hstack((np.zeros((X.shape[0],X.shape[1])),X))
            fullX = np.vstack((fullX_up,fullX_temo))
            O_up = np.zeros(K_nm.shape[0])
            fullO = np.concatenate([O_up,O])
            target = None
            charges_part1 = np.linalg.multi_dot([fullK_nm.T,fullX.T,fullA.T,T.T])
            charges_part2 = np.linalg.multi_dot([T,fullA,fullO])
            charges_part3 = np.linalg.multi_dot([T,fullA,fullX,fullK_nm])
            #LambdaTarget = np.linalg.inv(lambda_target*np.eye(len(ref)))
        

        LambdaE = np.linalg.inv(lambda_reg*np.eye(len(E_ref)))
        A_ones = np.zeros((len(self.Kernel.training_set_descriptors),len(self.Kernel.training_set)))
        q_0 = get_charges(self.Kernel.training_set,charge_keyword=charge_keyword)
        Cs, Qmatrix = self._process_ref_energy(q_0)
        count_c = 0
        count_r = 0
        for mol in self.Kernel.training_set:
            for at in mol:
                A_ones[count_r,count_c] = 1
                count_r += 1
            count_c += 1
        train_en = True
        train_charges = False
        count_iter = 0
        bad = 1
        while train_en:
            M = np.vstack((A_ones,Qmatrix))
            up = np.linalg.multi_dot([fullK_nm.T,M,LambdaE,Cs]) - np.linalg.multi_dot([fullK_nm.T,M,LambdaE,E_ref])
            down = -np.linalg.multi_dot([fullK_nm.T,M, LambdaE,M.T,fullK_nm])
            if train_charges:
            
                Lambda_charges = ((1/lambda_charges)*np.eye(len(ref)))
                print("Target regularization:", lambda_charges)
                up += np.linalg.multi_dot([charges_part1,Lambda_charges,ref])
                up += np.linalg.multi_dot([charges_part1,Lambda_charges,charges_part2])
                down -= np.linalg.multi_dot([charges_part1,Lambda_charges,charges_part3])
                lambda_charges = lambda_charges/2
                if lambda_charges < lambda_charges_min:
                    lambda_charges = lambda_charges_min
                
            down -= fullK_mm

            weights = np.linalg.solve(down,up)
            WkQEQ = weights[K_nm.shape[1]:] 
            eneg  = np.matmul(K_nm,WkQEQ)
            eneg_tot = (np.matmul(X,np.transpose(eneg)))
            eneg_tot = eneg_tot + O
            charge_temp = np.matmul(A,-eneg_tot)
            q_new = np.matmul(Xback,charge_temp)
            ref = q_new
            Cs_temp, Qmatrix_temp = self._process_ref_energy(q_new)
            M_temp = np.vstack((A_ones,Qmatrix))
            whole_energy =  np.linalg.multi_dot([M_temp.T,fullK_nm,weights])
            
            whole_energy = Cs_temp + whole_energy
            MSE = np.square(np.subtract(E_ref,whole_energy)).mean()  
            RMSE_E = np.sqrt(MSE)
            print("New RMSE:",RMSE_E)
            print("Old RMSE:",oldRMSE)
            print("Bad RMSEs in row:", bad)
            if RMSE_E > oldRMSE and train_charges == False:
                train_charges = True
                print("Charges added")
            elif RMSE_E > oldRMSE and train_charges == True and bad >= iter_charges:
                train_en = False
            elif RMSE_E > oldRMSE and train_charges == True and bad < iter_charges:
                bad += 1
                Cs = Cs_temp
                Qmatrix = Qmatrix_temp
                ref = q_new
            else:
                bad = 0
                final_weight = weights
                ref = q_new
                oldRMSE = RMSE_E
                Cs = Cs_temp
                Qmatrix = Qmatrix_temp
            print("Sample of charges:",q_new[-10:])
            print(f"iteration {count_iter} is done")
            count_iter += 1
        self.weights = final_weight 


    def calculate(self, mol, charge=0):
        """Calculates charges, energy, forces and dipole_vector for single atoms object. This is usable with trainEnergy function. After training electronegativities do not correspond with charges obtained via qe.calc_Charges()

        Parameters
        ----------
            mol : obj
                Atoms object
            charge : int
                Charge of mol

        Returns
        -------
            results: dict
                Dictionary of results

        """
        K = self.Kernel.kernel_matrix(mol_set1 = [mol],kerneltype="predicting")
        gapW = self.weights[:K.shape[1]]
        kQEqW = self.weights[K.shape[1]:]
        energyGAP = np.matmul(K,gapW)
        #print("GAP: ", np.sum(energyGAP)*Hartree)
        eneg   = np.matmul(K,kQEqW)
        
        qe     = charge_eq(mol,Qtot=charge,eneg=eneg,scale_atsize=self.scale_atsize,DoLJ=self.LJ,radius_type=self.radius_type, hard=self.hard_lib,periodic = self.periodic)
        qe.calc_Charges()
        qe.calc_Eqeq()
        mu = np.matmul(_get_R(mol),qe.q)
        energy  = qe.E 
        #print("kQEq:", energy*Hartree)
        #print("GAP:",np.sum(energyGAP)*Hartree)
        return np.sum(energyGAP)*Hartree + energy*Hartree, qe.q, mu#, np.sum(energyGAP)*Hartree, energy*Hartree
