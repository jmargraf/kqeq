
import numpy as np
from ase.units import Hartree,Bohr
from ase.data import covalent_radii, chemical_symbols
from scipy.special import erf
from qpac.data import uff_xi_vdw, uff_Di_vdw, uff_Xi_qeq, uff_radius_qeq
from qpac.periodic import eemMatrixEwaldpy,eemMatrixEwald,derA,getReal, getRecip, derApy
# import ewald

#               H             Li            C             N             O
#eneg_default = {1:4.5/Hartree,3:4.5/Hartree,6:5.3/Hartree,7:6.9/Hartree,8:8.7/Hartree}

kcal2hartree = 0.0015936010974213599

class charge_eq():
    def __init__(self,mol,Qtot=0.0,q=None,eneg=[],hard=None,atsize=None,scale_atsize=1.0,DoLJ=False,radius_type='qeq',periodic = False,ext_field=[]):
        """
        initialization and precomputation
        """
        # inputs and defaults
        self.xyz = mol.get_positions()/Bohr
        self.atoms = mol.get_atomic_numbers()
        self.nAt = len(self.atoms) 
        self.Qtot = Qtot
        self.DoLJ = DoLJ
        if len(eneg) == 0:
            eneg = []
            for El in self.atoms:
                eneg.append(uff_Xi_qeq[chemical_symbols[El]]/Hartree)
            self.eneg = np.array(eneg)
        else:
            self.eneg = eneg

        if len(ext_field) == 0:
            # field has to be provided in the V/A, which should be consistent with settings from FHI-AIMS
            self.ext_field = np.array([0,0,0])
            self.dip_ext_field = np.einsum("ij,j->i",self.xyz,self.ext_field)
        else:
            self.ext_field = np.array(ext_field)/(Hartree/Bohr) # from V/A to a.u.
            self.dip_ext_field = 1*np.einsum("ij,j->i",self.xyz,self.ext_field)

        # print("dip",self.dip_ext_field)
        # print("eneg",self.eneg)
        self.periodic = periodic
        
        if self.periodic == True:
            self.lattice = mol.cell/Bohr


        if q is None:
            self.q = np.zeros(self.nAt)
        else:
            self.q = q



        if hard == None:
            self.hard = np.zeros(self.nAt)
        else:
            hard_temp = []
            for El in self.atoms:
                hard_temp.append(hard[chemical_symbols[El]]/Hartree)
            self.hard = np.array(hard_temp)


        if atsize == None:
            atsize = []
            for El in self.atoms:
                if radius_type=='rcov':
                    atsize.append(covalent_radii[El]/Bohr) 
                elif radius_type=='qeq':
                    atsize.append(uff_radius_qeq[chemical_symbols[El]]/Bohr)
            self.atsize = scale_atsize*np.array(atsize)
        else:
            self.atsize = atsize
        if self.DoLJ:
            xi_LJ = [] 
            Di_LJ = []
            for El in self.atoms:
                xi_LJ.append(uff_xi_vdw[chemical_symbols[El]]/Bohr)
                Di_LJ.append(uff_Di_vdw[chemical_symbols[El]]*kcal2hartree)
            self.xi_LJ = np.array(xi_LJ)
            self.Di_LJ = np.array(Di_LJ)
        
        # calc distance matrix
        self.calc_Rij()

        self.E = None
    '''
    def calc_Rij(self):
        self.Rij = np.zeros((self.nAt,self.nAt))
        for i in range(self.nAt):
            for j in range(self.nAt):
                self.Rij[i,j] = self.calc_Rvec(i,j)
    '''
    def get_Recip(self):
        A = getRecip(self.nAt, self.xyz,self.lattice,self.atsize, self.hard)
        return A

    def get_Real(self):
        A = getReal(self.nAt, self.xyz,self.lattice,self.atsize, self.hard)
        return A
    
    def calc_Rij(self):
        self.Rij = np.sqrt(np.sum((self.xyz[:,np.newaxis,:]-self.xyz[np.newaxis,:,:])**2,axis=-1))
    
    def calc_Rvec(self,i,j):
        vi = self.xyz[i]
        vj = self.xyz[j]
        rij = np.subtract(vj,vi)
        r = np.linalg.norm(rij)
        return r


    def calc_dVdr(self,ga_ij,Rij):
        dVdr = 0.0
        if Rij>0.0:
            dVdr += 2.0*ga_ij*np.exp(-ga_ij**2 * Rij**2)/(np.sqrt(np.pi)*Rij)
            dVdr += -erf(ga_ij*Rij)/(Rij**2)
        return dVdr

    def calc_ELJ(self):
        etmp = 0.0
        for i in range(self.nAt):
            for j in range(i):
                rvec = self.xyz[i] - self.xyz[j]
                rij = np.linalg.norm(rvec)
                
                Dij = np.sqrt(self.Di_LJ[i]*self.Di_LJ[j])
                xij = np.sqrt(self.xi_LJ[i]*self.xi_LJ[j])
                xr6  = (xij/rij)**6
                xr12 = xr6**2
                etmp += Dij*(-2.*xr6+xr12)
                
                #Forces
                dVdr = 12.*(Dij/rij)*(xr6-xr12)
                self.f[i,:] += -dVdr/rij * rvec
                self.f[j,:] +=  dVdr/rij * rvec

        self.E += etmp
    
    def calc_gamma(self,a1,a2):
        return 1.0/np.sqrt(a1**2+a2**2)

    def calc_Vscreen(self,ga_ij,Rij):
        return erf(ga_ij*Rij)/Rij
    
    def calc_Eqeq(self):
        A = self.get_A()
        self.E = self.q@(self.eneg-self.dip_ext_field) + 0.5*self.q@A@self.q

    # def calc_Eqeq_ext_field(self):
    #     A = self.get_A()
    #     self.E = self.q@(self.eneg-self.dip_ext_field) + 0.5*self.q@A@self.q
    #     self.E -= self.dip_ext_field@self.q

    def calc_EqeqPart(self):
        A = self.get_A()
        self.E = 0.5*self.q@A@self.q

    def calc_EqeqExt(self):
        return -self.dip_ext_field@self.q
        
    def calc_Fqeq(self):
        if self.periodic == False:
            dAdr = self.get_dAdr()
            f = -1*np.einsum("i,j,ijd->id",self.q,self.q,dAdr)
            ext_force = self.q[:, np.newaxis] * self.ext_field
            self.f = f + ext_force
        elif self.periodic == True:
            dA = self.get_dA()
            self.f = np.zeros((self.nAt,3))
            for a in range((self.nAt)):
                self.f -= dA[a]*self.q[a]



    def calc_ChemPot(self,i):
        #A = self.atoms[i]
        #para = self.par[A]
        qi = self.q[i]
        Xi = self.eneg[i]
        for j in range(self.nAt):
            qj = self.q[j]
            ga_ij = self.calc_gamma(self.atsize[i],self.atsize[j])
            rij = self.Rij[i,j]
            Xi += self.calc_Vscreen(i,j)*qj
        return Xi


    def get_A(self):
        if self.periodic == True:
            A = eemMatrixEwald(self.nAt, self.xyz,self.lattice,self.atsize, self.hard)
        elif self.periodic == False:
            Rij = self.Rij
            eye = np.eye(Rij.shape[0])
            Rij = Rij + eye
            antieye = np.ones_like(Rij) - eye
            ga_ij = 1.0/(np.sqrt(2)*np.sqrt(self.atsize[:,np.newaxis]**2 + self.atsize[np.newaxis,:]**2))
            Vscreen = erf(ga_ij*Rij)/Rij
            A = Vscreen * antieye + eye*(self.hard + 1/(self.atsize*np.sqrt(np.pi)))
        return A

    def get_Abar(self):
        A = self.get_A()
        A = np.row_stack((A, np.ones(self.nAt)))
        A = np.column_stack((A, np.ones(self.nAt+1)))
        A[-1,-1] = 0
        return A
    
    def get_Ainv(self):
        A = self.get_Abar()
        Ainv = np.linalg.inv(A)
        return Ainv


    def get_dA(self):
        '''
        This returns dA/dr, where r is taken as a scalar. dA here is N,N, where N is a number of atoms
        '''
        if self.periodic == False:
            Rij = np.sqrt(np.sum((self.xyz[:,np.newaxis,:]-self.xyz[np.newaxis,:,:])**2,axis=-1)) # see scipy distance matrix
            eye = np.eye(Rij.shape[0])
            Rij = Rij + eye
            ga_ij = 1.0/np.sqrt(self.atsize[:,np.newaxis]**2 + self.atsize[np.newaxis,:]**2)
            dVdr = (Rij*np.sqrt(2)*ga_ij*np.exp(-(ga_ij**2 * Rij**2)/2)-np.sqrt(np.pi)*erf(ga_ij*Rij/np.sqrt(2)))/(np.sqrt(np.pi)*Rij*Rij*Rij)
        elif self.periodic == True:
            dVdr = derA(self.nAt, self.xyz,self.lattice,self.atsize, self.q)#(nat,ats,lat,sigma,q)()
        return dVdr

    def get_dAdr(self):
        '''
        return dA/dr, where r is a vector
        dimension are N (which atom), N (wrt derivatives), 3 (x,y,z coordinates)
        '''
        dA = self.get_dA()
        r = self.xyz[:, np.newaxis, :] - self.xyz 
        dAdr = r*dA[:, :, np.newaxis]
        return dAdr
    
    def calc_Charges(self):
        C = np.zeros(self.nAt+1)
        C[:-1] = -(self.eneg-self.dip_ext_field)
        C[-1] = self.Qtot
        Ainv = self.get_Ainv()
        q = np.matmul(Ainv,C) # np.einsum("ij,j",Ainv,C)
        self.q = q[:-1]

    def compConst(self,charges):
        A = self.get_Abar()
        A = A[:-1,:-1]
        char_arr = np.array(charges)
        C = 0.5*char_arr@A@char_arr
        return C


    def min_Charges(self):
        from scipy.optimize import minimize
        q = self.q
        cons = ({'type': 'eq', 'fun': lambda x:  np.sum(x)-self.Qtot})
        res = minimize(self.fun_Etot, q, method='SLSQP', constraints=cons,
                       options={'maxiter': 500, 'ftol': 1e-08, 'iprint': 1, 'disp': True, 'eps': 1.4901161193847656e-08})
        self.q = res.x

    def fun_Etot(self):
        q = self.q 
        #print("calculating electrostatic energy")
        #print(self.q)
        #self.E = 0.0
        E = 0.0
        for i in range(len(q)):
            #A = self.atoms[i]
            #para = self.par[A]
            qi = q[i]
            ga_ii = self.calc_gamma(self.atsize[i],self.atsize[i])

            E_1 = self.eneg[i]*qi
            E_2 = 0.5*(self.hard[i]+2.0*ga_ii/np.sqrt(np.pi))*qi**2
            E_3 = 0 
            for j in range(i):
                #B = self.atoms[j]
                #parb = self.par[B]
                qj = q[j]
                ga_ij = self.calc_gamma(self.atsize[i],self.atsize[j])
                rij = self.Rij[i,j]

                E_3 += qi*qj*self.calc_Vscreen(ga_ij,rij)
        print(E_1,E_2,E_3)
    
    def calculate(self):
        self.calc_Eqeq()
        self.calc_Fqeq()
        return {"energy":self.E, "forces":self.f}
