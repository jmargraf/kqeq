import random
import numpy as np

import matplotlib.pyplot as plt
def plot_basics(ref, 
                kqeq, 
                title = None,
                xlabel = None, 
                ylabel = None, 
                preset="dipole", 
                size_fig = (8,5), 
                fig_dpi = 100,
                xlim = [-8,8], 
                ylim = [-8,8], 
                save = None):
    if preset == "dipole":
        xlabel = 'Reference Dipole Vector Elements'
        ylabel = 'kQEq Dipole Vector Elements'
    elif preset == "charges":
        xlabel = 'Reference charges'
        ylabel = 'kQEq charges'
    elif preset == "energy":
        xlabel = "Reference DFT Energy per atom / eV"
        ylabel = 'kQEq Energy per atom / eV'
    elif preset == "forces":
        xlabel = "Reference DFT force eV/A"
        ylabel = 'kQEq Force eV/A'
    plt.figure(figsize=size_fig, dpi=fig_dpi)
    plt.scatter(ref,kqeq,alpha = 0.5)
    plt.plot(ref,ref,color="black")
    plt.xticks(fontsize=18)
    plt.yticks(fontsize=18)
    plt.title(title,fontsize=26)
    plt.xlabel(xlabel,fontsize=22)
    plt.ylabel(ylabel,fontsize=22)
    if save is not None:
        plt.savefig(save,bbox_inches='tight')
    plt.show()

