import numpy as np

def addPeriodicity(mols):
    for mol in mols:
        xyz = mol.get_positions()
        x = abs(max(xyz[:,0]) - min(xyz[:,0]))
        y = abs(max(xyz[:,1]) - min(xyz[:,1]))
        z = max(xyz[:,2]) -  min(xyz[:,2])
        mol.set_cell((x*10, y*10, z*10))
        mol.set_pbc((True, True, True))
        mol.center()

def num_force(mol,my_kqeq,h=0.0001,direction=0,iatom=0):
    tmpmol = mol.copy()
    pos = tmpmol.get_positions()#/Bohr
    pos[iatom][direction] += h
    tmpmol.set_positions(pos)#*Bohr)
    res = my_kqeq.calculateEQ(tmpmol)
    Eplus = res['energy']#/Hartree
    pos[iatom][direction] += -2.0*h
    tmpmol.set_positions(pos)#*Bohr)
    res = my_kqeq.calculateEQ(tmpmol)
    Eminus = res['energy']#/Hartree
    pos[iatom][direction] += h
    tmpmol.set_positions(pos)#*Bohr)
    energy = (Eplus-Eminus)/(2.0*h)#*Bohr)
    return energy#*Hartree#/Bohr
    
def num_forces(mol,my_kqeq,h=0.0001):
    f = np.zeros((len(mol),3))
    for i in range(len(mol)):
        for direction in range(3):
            f[i,direction] = -num_force(mol,my_kqeq,h=h,direction=direction,iatom=i)
    return f

def num_descriptor(mol,descriptor,h=0.0001,direction=0,iatom=0):
    tmpmol = mol.copy()
    pos = tmpmol.get_positions()#/Bohr
    pos[iatom][direction] += h
    tmpmol.set_positions(pos)#*Bohr)
    Dplus,elements_list = descriptor.descriptor_atoms([tmpmol])
    pos[iatom][direction] += -2.0*h
    tmpmol.set_positions(pos)#*Bohr)
    Dminus,elements_list = descriptor.descriptor_atoms([tmpmol])
    pos[iatom][direction] += h
    tmpmol.set_positions(pos)#*Bohr)
    energy = (Dplus-Dminus)/(2.0*h)#*Bohr)
    return energy#*Hartree#/Bohr
    
def num_descriptors(mol,my_kqeq,h=0.0001):
    f = np.zeros((len(mol),3))
    for i in range(len(mol)):
        for direction in range(3):
            f[i,direction] = -num_descriptor(mol,my_kqeq,h=h,direction=direction,iatom=i)
    return f
