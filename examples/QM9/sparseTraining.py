import sys
sys.path.append("/datavon1/dev_qpac_fitting_forces/qpac/")


import matplotlib.pyplot as plt
from qpac.descriptors import dscribeSOAP, quipSOAP
from qpac.kernel import *
from qpac.utils import *
from qpac.kQEq import kernel_qeq 

from ase.units import Hartree
from ase.io import read, write

import time

import numpy as np

def calc_MAE(dip1,dip2):
    return np.mean(np.abs(dip1-dip2))

def calc_MAE_abs_dipole_debye(dip1,dip2):
    n_dipoles = int(len(dip1)/3)
    dipoles1 = np.reshape(dip1,(n_dipoles,3))
    dipoles2 = np.reshape(dip2,(n_dipoles,3))
    return (np.mean(np.abs(np.linalg.norm(dipoles1,axis=1)-np.linalg.norm(dipoles2,axis=1))))*2.541746229
def calc_RMSE(dip1,dip2):
    n_dipoles = int(len(dip1)/3)
    dipoles1 = np.reshape(dip1,(n_dipoles,3))
    dipoles2 = np.reshape(dip2,(n_dipoles,3))
    MSE_D = np.square((np.linalg.norm(dipoles1,axis=1)-np.linalg.norm(dipoles2,axis=1))*2.541746229).mean() 
    RMSE_D = np.sqrt(MSE_D)
    return RMSE_D

random.seed(1) # other seeds for models were 2, and 3
train_set_all = read("QM9train.xyz@:200",format="extxyz")
val_set = read("QM9val.xyz@:100",format="extxyz")
test_set = read("QM9test.xyz@:100",format="extxyz")
random.shuffle(train_set_all)

val_dipoles = get_dipoles(val_set)
test_dipoles = get_dipoles(test_set)
species = np.unique([spec  for mol in train_set_all for spec in  mol.get_chemical_symbols()])

rcut = 2.6
ntrain = 100 # int(sys.argv[1])
nsparse = 500 # int(sys.argv[2])
sigma = 0.1 # float(sys.argv[3])


train_set = train_set_all[:ntrain]

soap_desctipor = quipSOAP(STRsoap="soap cutoff=3.0 l_max=5 n_max=6 atom_sigma=0.5 normalize=T n_species=5 species_Z={6 9 1 7 8} n_Z=5 Z={6 9 1 7 8}", species=species)

# SOAPclass = dscribeSOAP(nmax = 5,
#                    lmax = 3,
#                         delta=1.0,
#                         rcut = rcut,
#                     sigma = rcut/8,
#                     species = species,
#                     periodic = False)
soap_desctipor_dscribe = dscribeSOAP(nmax = 5,
                                     lmax = 3,
                                     rcut = 2.6,
                                     sigma = 2.6/8,
                                     delta=1.0,
                                     species = species,
                                     periodic = False)

qpac_kernel = qpacKernel(Descriptors=soap_desctipor,
                         training_set=train_set,
                         training_set_charges=[0 for a in train_set],
                         sparse=True,
                         perEl = True,
                         sparse_method = "CUR",
                         sparse_count=nsparse)

t0 = time.time()
my_kqeq = kernel_qeq(Kernel=qpac_kernel,
                     scale_atsize=1/np.sqrt(2.0),
                     radius_type="qeq")
t1 = time.time() - t0
my_kqeq.train(target_sigmas =[sigma],#,0.01], 
              targets = ["dipole"],#,"charges"], 
              charge_keyword="hirshfeld")
t2 = time.time() - t0
val_res = my_kqeq.predict(val_set)
t3 = time.time() - t0
test_res = my_kqeq.predict(test_set)
t4 = time.time() - t0

mae_val   = calc_MAE_abs_dipole_debye(val_res["dipoles"],val_dipoles)
mae_test = calc_MAE_abs_dipole_debye(val_res["dipoles"],test_dipoles)
RMSE_val   = calc_RMSE(val_res["dipoles"],val_dipoles)
RMSE_test = calc_RMSE(val_res["dipoles"],test_dipoles)
t5 = time.time() - t0
my_kqeq.save_kQEq()
print(nsparse,",",ntrain,",",sigma,",",mae_val,",",mae_test,",",RMSE_val,",",RMSE_test,",",t1,",",t2,",",t3,",",t4,",",t5)
import matplotlib.pyplot as plt
plt.scatter(test_res["dipoles"],test_dipoles)
plt.plot(test_dipoles,test_dipoles,color="black")
plt.show()

# 500 , 100 , 0.1 , 0.4027294283935196 , 1.9315763879716854 , 0.6017038594959229 , 2.420351660295171 , 0.34710025787353516 , 0.8372828960418701 , 1.7924315929412842 , 2.6093647480010986 , 2.609656572341919
