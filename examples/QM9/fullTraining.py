
import matplotlib.pyplot as plt


from qpac.descriptors import dscribeSOAP
from qpac.kernel import *
from qpac.utils import *
from qpac.kQEq import kernel_qeq 

from ase.units import Hartree
from ase.io import read, write

import time

import numpy as np

def calc_MAE(dip1,dip2):
    return np.mean(np.abs(dip1-dip2))

def calc_MAE_abs_dipole_debye(dip1,dip2):
    n_dipoles = int(len(dip1)/3)
    dipoles1 = np.reshape(dip1,(n_dipoles,3))
    dipoles2 = np.reshape(dip2,(n_dipoles,3))
    return (np.mean(np.abs(np.linalg.norm(dipoles1,axis=1)-np.linalg.norm(dipoles2,axis=1))))*2.541746229
def calc_RMSE(dip1,dip2):
    n_dipoles = int(len(dip1)/3)
    dipoles1 = np.reshape(dip1,(n_dipoles,3))
    dipoles2 = np.reshape(dip2,(n_dipoles,3))
    MSE_D = np.square((np.linalg.norm(dipoles1,axis=1)-np.linalg.norm(dipoles2,axis=1))*2.541746229).mean() 
    RMSE_D = np.sqrt(MSE_D)
    return RMSE_D

random.seed(1) # other seeds for models were 2, and 3
train_set_all = read("QM9train.xyz@:",format="extxyz")
val_set = read("QM9val.xyz@:",format="extxyz")
test_set = read("QM9test.xyz@:",format="extxyz")
random.shuffle(train_set_all)

val_dipoles = get_dipoles(val_set)
test_dipoles = get_dipoles(test_set)
species = np.unique([spec  for mol in train_set_all for spec in  mol.get_chemical_symbols()])

rcut = 4.4
ntrain = 500 # int(sys.argv[1])
nsparse = 500 # int(sys.argv[2])
sigma = 0.1 # float(sys.argv[3])


train_set = train_set_all[:ntrain]
SOAPclass = dscribeSOAP(nmax = 5,
                   lmax = 3,
                    rcut = rcut,
                    sigma = rcut/8,
                    species = species,
                    periodic = False)
  
SOAP_Kernel = SOAPKernel(multi_SOAP=False,
                Descriptor=SOAPclass,
                training_set=train_set,
                training_system_charges=[0 for a in train_set],
                perEl = True,
                sparse=False)

t0 = time.time()
my_kqeq = kernel_qeq(Kernel=SOAP_Kernel,
                     scale_atsize=1/np.sqrt(2.0),
                     radius_type="qeq")
t1 = time.time() - t0
my_kqeq.train(target_lambdas =[sigma],#,0.01], 
              targets = ["dipole"],#,"charges"], 
              charge_keyword="hirshfeld")
t2 = time.time() - t0
kqeq_dipoles_val,kqeq_charges_val, _ = my_kqeq.predict(val_set)
t3 = time.time() - t0
kqeq_dipoles_test,kqeq_charges_test, _ = my_kqeq.predict(test_set)
t4 = time.time() - t0
mae_val   = calc_MAE_abs_dipole_debye(kqeq_dipoles_val,val_dipoles)
mae_test = calc_MAE_abs_dipole_debye(kqeq_dipoles_test,test_dipoles)
RMSE_val   = calc_RMSE(kqeq_dipoles_val,val_dipoles)
RMSE_test = calc_RMSE(kqeq_dipoles_test,test_dipoles)
t5 = time.time() - t0
my_kqeq.save_kQEq()
print("full ",",",ntrain,",",sigma,",",mae_val,",",mae_test,",",RMSE_val,",",RMSE_test,",",t1,",",t2,",",t3,",",t4,",",t5)
import matplotlib.pyplot as plt
plt.scatter(kqeq_dipoles_test,test_dipoles)
plt.plot(test_dipoles,test_dipoles)
plt.show()
