from qpac.descriptors import dscribeSOAP
from qpac.kernel import *
from qpac.utils import *
from qpac.kQEq import kernel_qeq

from ase.units import Hartree
from ase.io import read, write

import numpy as np


# Commented structures are the ones used for the training and evaluation of the final model in the paper. This requires larger amount of RAM and the training time 
'''
train_set = read("particles.xyz@:",format="extxyz")
temp = read("particles1.xyz@:", format="extxyz")
train_set.extend(temp[:50])
temp = read("particles2.xyz@:", format="extxyz")
train_set.extend(temp[:100])
temp = read("particles3.xyz@:", format="extxyz")
train_set.extend(temp[:200])
temp = read("particles4.xyz@:", format="extxyz")
train_set.extend(temp[:200])

test_set = read("particles5.xyz@:", format="extxyz")[:200]
'''
##########################################################
temp = read("particles4.xyz@:",format="extxyz")
train_set = temp[:50]
test_set = read("particles5.xyz@:",format="extxyz")



atom_energy = {"Zn": -49117.02929728, "O": -2041.3604}

hard_lib = {"O": 1*Hartree, "Zn": 1*Hartree}


SOAPclass = dscribeSOAP(nmax = 6,
                   lmax =5 ,
                    rcut = 3.0,
                    sigma = 3.0/8,
                    species = ["Zn","O"],
                    periodic = False)


SOAP_Kernel = SOAPKernel(multi_SOAP=False,
                     Descriptor=SOAPclass,
                     training_set=train_set,
                     sparse=True,
                     sparse_method="CURel",
                     sparse_count=500,
                     perEl = False,
)

# Energy of the training set
ref_en_train = get_energies(mols=train_set,atom_energy = atom_energy)

# Create an instance of the kQEq class
my_kqeq = kernel_qeq(Kernel=SOAP_Kernel,
                     scale_atsize=1.0/np.sqrt(2),
                     radius_type="qeq",
                     hard_lib=hard_lib)

my_kqeq.trainEnCharge(atom_energy=atom_energy,
                      charge_keyword="hirshfeld", 
                      lambda_reg = 0.01,
                      lambda_charges = 0.01,
                      lambda_charges_min=0.001,
                      iter_charges = 2)

my_kqeq.save_kQEq()

ref_en_test = get_energies_perAtom(mols=test_set,atom_energy = atom_energy)
ref_q_test = get_charges(mols=test_set, charge_keyword="hirshfeld")
E_test=[]
q_test = []
for a in test_set:
    temp = my_kqeq.calculateEQ(a)
    E_test.append(temp["energy"]/len(a))
    q_test.extend(temp["charges"])
plot_basics(ref=ref_en_test,kqeq = E_test,preset="energy")
plot_basics(ref=ref_q_test,kqeq = q_test,preset="charges")

